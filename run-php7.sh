#!/bin/bash

# Launch php inside a Debian container (to be run on the host)

# uncomment if unsure to get debug messages
#set -x

# Whatever you named that image
# See https://gitlab.com/olberger/local-php7-sqlite-debian for mine
DOCKERIMAGE=olberger/local-php7-sqlite-debian:bullseye-slim

PHP_USER=phpuser


usage()
{
cat << EOF
usage: $0 [options] [COMMAND]

   COMMAND  (optional) command to be executed inside the container

OPTIONS:
   -n NAME                          Name of the running container
   -p PORT                          Local port bound to the container
   -?                               Show this message
   -v                               displays verbose messages

DESCRIPTION

   Without arguments, runs php -S on localhost:8000

EOF
}

verbose=0
port=
run_background=0
# stop_server=0
container_name=

while getopts 'vbn:p:' OPTION; do
    case $OPTION in
        'b')
            run_background=1
            ;;
        # 'k')
        #     stop_server=1
        #     ;;
        'p')
            port=${OPTARG}
            if [[ -z "${port##*[!0-9]*}" ]] ; then
                echo "Invalid argument for -p: "$OPTARG"" >&2
                usage
                exit 1
            fi
            ;;
        'n')
            container_name=${OPTARG}
            # if [[ -z "${port##*[!0-9]*}" ]] ; then
            #     echo "Invalid argument for -p: "$OPTARG"" >&2
            #     usage
            #     exit 1
            # fi
            ;;
        'v')
            verbose=1
            ;;
	\?) echo "Invalid option: -"$OPTARG"" >&2
            usage
            exit 1
            ;;
        : ) echo "Option -"$OPTARG" requires an argument." >&2
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

if [ $verbose -ne 0 -a "x$port" != "x" ] ; then
    echo "Bind port $port"
fi

# Define env vars

# The container will run process with same ids as the launching user
# on the host. This ensures that files generated in the current dir,
# which is bind-mounted inside the container, will have correct owner
USER_UID=$(id -u)
USER_GID=$(id -g)


# Or just run php with a provided script maybe
if [ $# -ne 0 ]; then
    ARGS="$*"
else
    # Unless arguments are explicitely provided, start php -S to start embedded Web server
    ARGS="php -S 0.0.0.0:8000"
    port=8000
fi

ENV_VARS+=" --env=USER_UID=${USER_UID}"
ENV_VARS+=" --env=USER_GID=${USER_GID}"

#set -x

if [[ -z "${PHP_HOME}" ]]; then
  PHP_HOME=$(dirname $(realpath $0))/temp-php_home # Default directory
fi

mkdir -p ${PHP_HOME}/dot_composer

if [ $verbose -ne 0 ] ; then
    echo "Using PHP_HOME: ${PHP_HOME}"
fi

VOLUMES+=" --volume=${PHP_HOME}/dot_composer:/home/${PHP_USER}/.composer"

if [ "x$port" != "x" ]; then
    PORTS="-p $port:$port"
fi
#exit 0

TERM_MODE="-t -i"

RUN_MODE="--rm"
if [ $run_background -eq 1 ]; then
    TERM_MODE=""
fi

CONTAINER_NAME=
if [ "x$container_name" != "x" ]; then
    CONTAINER_NAME="--name $container_name"
fi

# Start a container
# Options are :
#  --rm : destroy terminated container after run (make sure program's
#         outputs are saved inside /php, i.e. current dir)
#  -t -i : interactive terminal (we need stdout and more maybe)
#  -v "`pwd`:/php" : mount local directory to /php inside the
#         container (see Dockerfile's WORKDIR)
#  -p 8000:8000 : bind port 8000 (HTTP server's port) to same on the host
docker run ${RUN_MODE} ${TERM_MODE} ${ENV_VARS} ${VOLUMES} -v "`pwd`:/php" ${PORTS} ${CONTAINER_NAME} $DOCKERIMAGE $ARGS
