# Base of the image, Debian stable aka bullseye, at the time of writing
FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND noninteractive

# we install PHP 7 and necessary bits to be able to run composer (which in turn may use Git, or fetch over HTTPS)
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -qy --no-install-recommends \
    php7.4-cli \
    php-sqlite3 \
    php-intl \
    git \
    ca-certificates \
    php-zip \
    procps \
    sudo

# you may add additional needed modules
# RUN apt-get install -y --no-install-recommends \
#     php-ldap
RUN apt-get install -y --no-install-recommends \
    vim \
    composer \
    php-xml \
    php-curl \
    phpunit

# the current dir from where the container is run will be mounted inside /php
# typically we should be started with 'docker run ... -v "`pwd`:/php" ...' (see associated run-php5.sh)
WORKDIR /php

COPY ./docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["display-help"]

